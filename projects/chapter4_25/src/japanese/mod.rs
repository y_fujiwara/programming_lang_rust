//外部インターフェースの定義
//pub useで関数をモジュール(japanese)のスコープに持ち込む
pub use self::greetings::hello;
pub use self::farewells::goodbye;

mod greetings;
mod farewells;
