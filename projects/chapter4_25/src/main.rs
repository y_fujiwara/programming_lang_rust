extern crate chapter4_25 as sayings;

use sayings::japanese::greetings as ja_greetings;
use sayings::japanese::farewells::*;
// 中括弧部分は以下と同じ
// use sayings::english;
// use sayings::english::greetings as en_greetings;
// use sayings::english::farewells as en_farewells;
use sayings::english::{self, greetings as en_greetings, farewells as en_farewells};

fn main() {
    println!("Hello in English: {}", en_greetings::hello() );
    println!("Hello in Japanese: {}", ja_greetings::hello() );
    println!("Goodbye in English: {}", english::farewells::goodbye() );
    println!("Again: {}", en_farewells::goodbye() );
    println!("Again in Japanese: {}", goodbye() );
}

