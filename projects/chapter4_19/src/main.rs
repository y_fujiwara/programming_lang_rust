use std::fmt::Debug;

struct Circle {
    x: f64,
    y: f64,
    radius: f64,
}

trait HasArea {
    fn area(&self) -> f64;
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * (self.radius * self.radius)
    }
}

struct Square {
    x: f64,
    y: f64,
    side: f64,
}

impl HasArea for Square {
    fn area(&self) -> f64 {
        self.side * self.side
    }
}

// ジェネリクスのtrait境界
fn print_area<T: HasArea>(shape: T) {
    println!("This shape has an area of {}", shape.area());
}

// 構造体でのジェネリクスとtrait境界
struct Rectangle<T> {
    x: T,
    y: T,
    width: T,
    height: T,
}

// ==の比較のためPatialEqに限定する
impl<T: PartialEq> Rectangle<T> {
    fn is_square(&self) -> bool {
        self.width == self.height
    }
}

// 普通の複数境界
fn foo<T: Clone, K: Clone + Debug> (x: T, y: K) {
    x.clone();
    y.clone();
    println!("{:?}", y);
}

// 改行OK
fn bar<T, K> (x: T, y: K) 
    where T: Clone, 
          K: Clone + Debug {

    x.clone();
    y.clone();
    println!("{:?}", y);
}

trait ConvertTo<Output> {
    fn convert(&self) -> Output;
}

// i32をi64にコンバートする
impl ConvertTo<i64> for i32 {
    fn convert(&self) -> i64 { *self as i64 }
}

// T == i32の時に呼び出せる
fn normal<T: ConvertTo<i64>>(x: &T) -> i64 {
    x.convert()
}

fn inverse<T>() -> T
        // これはConvertTo<i64>であるかのように使っている
        where i32: ConvertTo<T> {
    42.convert()
}

// デフォルト実装
struct UseDefault;

trait Foo {
    fn is_valid(&self) -> bool;
    fn is_invalid(&self) -> bool { !self.is_valid() }
}

// そのままデフォルト実装を使うパターン
impl Foo for UseDefault {
    fn is_valid(&self) -> bool {
        println!("Called UseDefault.is_valid.");
        true
    }
}

struct OverrideDefault;

// デフォルト実装をオーバーライドするパターン
impl Foo for OverrideDefault {
    fn is_valid(&self) -> bool {
        println!("Called UseDefault.is_valid.");
        true
    }

    fn is_invalid(&self) -> bool {
        println!("Called UseDefault.is_invalid.");
        true
    }
}

fn main() {
    let circle = Circle {x: 1.0, y: 1.0, radius: 2.0};
    let square = Square {x: 0.0, y: 0.0, side: 1.0 };
    let mut r = Rectangle {
        x: 0,
        y: 0,
        width: 47,
        height: 47,
    };
    print_area(circle);
    print_area(square);
    assert!(r.is_square());

    r.height = 42;
    assert!(!r.is_square());

    foo("Hello", "world");
    bar("Hello", "world");

    let default = UseDefault;
    assert!(!default.is_invalid());

    let over = OverrideDefault;
    assert!(over.is_invalid());
}
