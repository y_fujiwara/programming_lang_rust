fn main() {
    // vectorはジェネリクスでどんな型でもとれる
    let mut v : Vec<i32> = vec![1, 2, 3, 4, 5];

    for i in &v {
        println!("A reference to {}", i); 
    }

    for i in &mut v {
        println!("A mutable reference to {}", i); 
    }

    for i in v {
        println!("A ownership reference to {}", i); 
    }
}
