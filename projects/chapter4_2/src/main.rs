fn main() {
    print_number(add_one(5));
    // ダイバージング関数は任意の型として扱える
    // let diverge: i32 = diverges();
    // diverges();

    // 関数ポインタ
    let f: fn(i32) -> i32 = plus_one;
    print_number(f(100));
}

fn print_number(x: i32) {
    println!("x is: {}", x);
}

// 戻り値有りの関数
fn add_one(x: i32) -> i32 {
    // 戻り値にはセミコロンがない
    // 式指向な言語なので他の言語のセミコロンと意味が違う
    x + 1
}

fn plus_one(i: i32) -> i32 {
    i + 1
}

fn diverges() -> ! {
    panic!("This function never returns!");
}
