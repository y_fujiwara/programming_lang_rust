use std::thread;
use std::time::Duration;
use std::sync::{Mutex, Arc};

// 哲学者構造体
struct Philosopher {
    name: String, // &strではなく,所有権を持つString型
    left: usize,
    right: usize,
}

// テーブル構造体
struct Table {
    forks: Vec<Mutex<()>>,
}

impl Philosopher {
    fn new(name: &str, left: usize, right: usize) -> Philosopher {
        Philosopher {
            name: name.to_string(),
            left: left,
            right: right,
        }
    }

    fn eat(&self, table: &Table) {
        // tableのフィールドの要素1つに対するロック
        // _leftと_rightがスコープから外れるとロックも外れる
        let _left = table.forks[self.left].lock().unwrap();
        thread::sleep(Duration::from_millis(150));
        let _right = table.forks[self.right].lock().unwrap();

        println!("{} is eating.", self.name);

        thread::sleep(Duration::from_millis(1000));

        println!("{} is done eating.", self.name);
    }
}

fn main() {
    let table = Arc::new(Table { forks: vec![
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
    ]});

    let philosophers = vec![
        Philosopher::new("Judith Butler", 0, 1),
        Philosopher::new("Gulles Deleuze", 1, 2),
        Philosopher::new("Karl Marx", 2, 3),
        Philosopher::new("Emma Goldman", 3, 4),
        Philosopher::new("Michel Foucault", 0, 4), // デッドロック対策
    ];

    // into_iterはphilosophersの各要素の所有権を持つイテレータを生成する
    // spawはクロージャを一つ取り、新しいスレッドでそれを実行する
    let handles: Vec<_> = philosophers.into_iter().map(|p| {
        let table = table.clone();

        thread::spawn(move || {
            p.eat(&table);
        })
    }).collect();

    for h in handles {
        // join呼び出しで各スレッド実行が完了するまで実行をブロックする
        h.join().unwrap();
    }
}
