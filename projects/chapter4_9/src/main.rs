struct Foo<'a> {
    x: &'a i32,
}

impl<'a> Foo<'a> {
    fn x(&self) -> &'a i32 { 
        self. x 
    }
}

fn main() {
    let y = &5; // let _y = 5; let y = &_yと等価
    let f = Foo { x: y };

    println!("x is {}", f.x());
}

// ライフタイム:黙示的
fn foo(x: &i32) {
}

// ライフタイム:明示的
fn bar<'a>(x: &'a i32) {
}

// ライフタイム省略の例一覧
fn print(s: &str); // 省略
fn print<'a>(s: &'a str); // 展開

fn debug(lvl: u32, s: &str); // 省略
fn debug<'a>(lvl: u32, s: &'a str); // 展開 lvlは参照ではない

fn substr(s: &str, until: u32) -> &str; // 省略
fn substr<'a>(s: &'a str, until: u32) -> &'a str; // 展開

// fn get_str() -> &str; // 不正 入力がない ３つのルールに当てはまらない省略

// fn frob(s: &str, t: &str) -> &str; // 不正 参照の入力が２つある
fn frob<'a, 'b>(s: &'a str, t: &'b str) -> &str; // 展開 出力ライフタイムが定まらない

fn get_mut(&mut self) -> &mut T; // 省略
fn get_mut<'a>(&'a mut self) -> &'a mut T; // 展開

fn args<T:ToCStr>(&mut self, args: &[T]) -> &mut Command; // 省略された形
fn args<'a, 'b, T:ToCStr>(&'a mut self, args: &'b [T]) -> &'a mut Command; // 展開された形

fn new(buf: &mut [u8]) -> BufWriter; // 省略された形
fn new<'a>(buf: &'a mut [u8]) -> BufWriter<'a>; // 展開された形
