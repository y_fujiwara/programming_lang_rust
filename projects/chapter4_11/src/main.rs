struct Point {
    x: i32,
    y: i32,
}

struct Point3d {
    x: i32,
    y: i32,
    z: i32,
}
fn main() {
    let origin = Point { x: 0, y: 0 };
    println!("The origin is at ({}, {})", origin.x, origin.y);

    let mut point = Point3d { x: 0, y: 0, z: 0 };
    println!("The point3d is at ({}, {}, {})", point.x, point.y, point.z);

    // アップデート
    point = Point3d { y: 1, ..point };
    println!("The update3d is at ({}, {}, {})", point.x, point.y, point.z);
}
