fn main() {
    // ミュータブル参照
    let mut x = 5;
    let y = &mut x; // yはmutではないので変更不可能

    // mutはパターンの一部
    let (mut x, y) = (5, 6);

    // 外側ミュータビリティ
    use std::sync::Arc;
    
    let x = Arc::new(5);
    let y = x.clone();
}
