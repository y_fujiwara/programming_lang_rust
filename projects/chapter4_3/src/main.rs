fn main() {
    // bool type
    let x = true;
    let y: bool = false;
    println!("bool型: {}, {}", x, y);

    // char type
    let c = 'c';
    let two_hearts = '💕';
    println!("char型: {}, {}", c, two_hearts);

    // 数値型
    let a = 10; // i32
    let b = 1.0; // f64
    println!("数値型: {}, {}", a, b);

    // 配列
    let list = [1, 2, 3]; // [i32; 3]
    let mut list2 = [1, 2, 3];
    list2[0] = 100;
    let list3 = [0; 20];
    println!("list型: {}, {}, {}", list[0], list2[0], list3.len());

    // スライス
    let slice = &list3[0..10];
    println!("スライス: {}", slice.len());

    // 文字列型
    let s = "str";
    println!("文字列型: {}", s);

    // タプル
    let t: (i32, &str, i32) = (1, "hello", 2);
    let (x, y, z) = t; // letの意味は分配束縛
    println!("タプル型: {}", t.0);
    println!("分配束縛後: {}, {}, {}", x, y, z);


}
