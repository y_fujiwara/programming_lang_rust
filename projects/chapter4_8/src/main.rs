fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = vec![1, 2, 3];
    // 参照渡しで渡す
    let answer = foo(&v1, &v2);
    println!("answer is {}", answer);

    // 参照の変更には&mutを使う
    // 元もmutじゃないと使えない
    // println!にxを与えている部分は通常の借用
    // 中括弧を消すと&mut xの部分とprintln!の部分で借用ルールがぶつかる
    let mut x = 5;
    {
        let y = &mut x;
        *y = 6;
    }
    println!("x is {}", x);
    
    let mut v = vec![1, 2, 3];
    // ここで参照を与えるので、forスコープ内では&mut参照はルールによって使えない
    for i in &v {
        println!("{}", i);
        // v.push(34); //error
    }
    
    // 生存期間のチェックによってエラーになる例
    // 宣言とは逆順に開放されるため、yが不正な参照になってしまう
    // let y: &i32;
    // let x = 5;
    // y = &x;

println!("{}", y);
}

// 参照渡しによる引数宣言
fn foo(v1: &Vec<i32>, v2: &Vec<i32>) -> i32 {
    // 変更以外の処理
    
    // 値を返す
    42
}
