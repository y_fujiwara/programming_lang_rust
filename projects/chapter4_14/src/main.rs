// パターンマッチについて
fn main() {
    let x = 'x';
    let c = 'c';

    match c {
        x => println!("x : {}, c : {}", x, c),
    }
    // シャドーイングによって外のxには影響を与えない
    println!("x : {}", x);

    let xx = 1;

    // パイプで複式マッチ
    match xx {
        1 | 2 => println!("one or two"),
        3 => println!("three"),
        _ => println!("anything"),
    }

    let xxx = 20;
    match xxx {
        e @ 20 ... 100 =>  println!("got a range element {}", e),
        _ => println!("anything"),
    }

    let x = 4;
    let y = false;

    // Guard
    match x {
        4 | 5 if y => println!("yes"),
        _ => println!("no"),
    }
}
