fn main() {
    let v = vec![1, 2, 3];
    // 別の変数への束縛はOK
    let v2 = v;
    // その後にvを使うのはNG
    // println!("v[0] is: {}", v[0]);

    // 関数呼び出しによる所有権の遷移
    let vv = vec![1, 2, 3];
    take(vv);
    // println!("vv[0] is: {}", vv[0]);

    // コピートレイトを実装しているプリミティブ型は再束縛しても使える
    let i = 21;
    let j = i;
    println!("i is: {}", i);
}

fn take(v: Vec<i32>) {
    // ムーブセマンティクスにとって何が起こっても関係ない
}
