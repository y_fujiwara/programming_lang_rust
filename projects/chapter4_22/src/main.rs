trait Foo {
    fn method(&self) -> String;
}

impl Foo for u8 {
    fn method(&self) -> String { format!("u8: {}", *self) }
}

impl Foo for String {
    fn method(&self) -> String { format!("string: {}", *self) }
}

// トレイト境界による静的ディスパッチ
// コンパイル後は各呼び出し先に合わせて特殊化したメソッドが作られる
// 特殊化された関数と呼ぶ
fn do_something<T: Foo>(x: T) {
    x.method();
}

// トレイトオブジェクトによる動的ディスパッチ
// キャスト
fn do_something2(x: &Foo) {
    x.method();
}

// トレイトオブジェクトによる動的ディスパッチ
// 型強制
fn do_something3(x: &Foo) {
    x.method();
}

pub struct TraitObject {
    // 実際のデータ ()は多分ユニット
    pub data: *mut (),
    // 仮想関数テーブル ()は多分ユニット
    pub vtable: *mut (),
}

struct FooVtable {
    destructor: fn(*mut ()),
    size: usize,
    align: usize,
    // 関数ポインタ 
    method: fn(*const ()) -> String,
}

fn call_method_on_u8(x: *const ()) -> String {
    let byte: &u8 = unsafe { &*(x as *const u8) };

    byte.method()
}

static Foo_for_u8_vtable: FooVtable = FooVtable {
    destructor: /* コンパイラマジック */,
    size: 1,
    align: 1,

    // 関数ポインタへキャスト
    method: call_method_on_u8 as fn(*const ()) -> String,
}

static call_method_on_String(x: *const ()) -> String {
    let string: &String = unsafe { &*(x as *const String) };

    string.method()
}

static Foo_for_String_vtable: FooVtable = FooVtable {
    destructor: /* コンパイラマジック */,
    size: 24,
    align: 8,

    // 関数ポインタへキャスト
    method: call_method_on_String as fn(*const ()) -> String,
}



fn main() {
    let x = 5u8;
    let y = "Hello".to_string();

    // 実際にはu8用に特殊化された関数のよびだし
    do_something(x);
    // 実際にはstring用に特殊化された関数のよびだし
    do_something(y);
    // キャストによる動的ディスパッチ
    do_something2(&x as &Foo);
    // 型強制による動的ディスパッチ
    do_something3(&y);

    let a: String = "foo".to_string(); 
    let xx: u8 = 1;

    // let b: &Foo = &a; 
    let b = TraitObject {
       // データを保存
       data: &a,
       // メソッドを保存
       vtable: &Foo_for_String_vtable 
    };

    // let y: &Foo = x;
    let y = TraitObject {
        // データを保存
        data: &xx,
        // メソッドを保存
        vtable: &Foo_for_u8_vtable
    };
    
    // b.method();
    (b.vtable.method)(b.data);
    
    // y.method();
    (y.vtable.method)(y.data);
}
