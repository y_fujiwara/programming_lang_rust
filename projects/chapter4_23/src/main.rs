#![feature(unboxed_closures)]
#![feature(fn_traits)]
use std::ops::{FnOnce, Fn};

// クロージャを引数に取る関数。動的ディスパッチしたいときも他のと同じ
fn call_with_one<F>(some_closure: F) -> i32 where F : Fn(i32) -> i32 {
    some_closure(1)
}

// クロージャで実際起こっていることは以下
struct Closure {
    i: isize,
}

impl FnOnce<(isize,)> for Closure {
    type Output = isize;

    extern "rust-call" fn call_once(self, (args,): (isize,)) -> Self::Output {
        self.i + args
    }
}
// ここまで

// クロージャを戻す関数
fn factory() -> Box<Fn(i32) -> i32> { // Fnのサイズは不定なので、Boxでトレイトオブジェクトの形式にする必要がある
    let num = 5;

    // moveしてコピーしないと、numがタングリングポインタになってしまう
    Box::new(move |x| x + num)
}

fn main() {
    // クロージャとムーブ
    let mut num = 5;

    {
        // moveすると値のコピーの所有権を取得するので、外側の元の変数には影響がない
        // move付きクロージャは独自のスタックを持つ
        // moveがない場合は、その時の環境のスタックに紐づく
        let mut add_num = move |x: i32| num += x;

        add_num(5);
    }

    let answer = call_with_one(|x| x + 2);

    // クロージャの中で起こっていること
    // 実際にクロージャの構文では構造体が環境に作られ、型名は匿名になる。
    let cls = Closure {i: 10};
    println!("{}", cls(199));

    println!("{}", num);
    println!("{}", answer);
    
    let f = factory();
    let answer2 = f(1); 
    println!("{}", answer2);
}
